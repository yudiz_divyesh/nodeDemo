var express = require('express'),
app = express(),
port = process.env.PORT || 3000;
app.listen(port);
console.log('Todo List RESTful API server started on: ' + port);

/*------------REQUIRED PACKAGE START-------------*/
var router = express.Router();
bodyParser = require('body-parser');
var urlparser = bodyParser.urlencoded({ extended: true });
var randomstring = require("randomstring");
var jwt = require('jsonwebtoken');
var nodemailer = require('nodemailer');
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3EfeqfJkorFlkj04&jkasfKfhJKfNBF';
//var db = require('./db.js');
/*------------REQUIRED PACKAGE END-------------*/

/*-------DB Connection Start--------*/
var mysql = require('mysql');

var con = mysql.createConnection({
  	host: "localhost",
 	user: "root",
  	password: "root",
  	port: '8889',
	database: 'demoNode'
});

con.connect(function(err) {
  	if (err) throw err;
  	console.log("Connected!");
});
/*-------DB Connection End--------*/


/*-----------------------------------FORGOT PASSWORD START---------------------------------------*/
app.post('/forgotPassword', urlparser, function(req,res) {
	
	var emailAddress = req.body.email;

	/*-------Email Connection Start--------*/
	var transporter = nodemailer.createTransport({
	    host: 'smtp.gmail.com',
	    service: "Gmail",
	    port: 465,
	    //secure: true, // secure:true for port 465, secure:false for port 587
	    auth: {
	        user: 'divyesh.gangani@ping2world.com',
	        pass: 'divyesh@123'
	    }
	});
	/*-------Email Connection Start--------*/

	/*-----------Random Number Start---------*/
	var randPass = randomstring.generate({
  		length: 6,
  		charset: 'numeric'
	});
	/*-----------Random Number End----------*/

	// setup email data with unicode symbols
	var mailOptions = {
	    from: '"divyesh.gangani@ping2world.com" <divyesh.gangani@ping2world.com>', // sender address
	    to: emailAddress, // list of receivers
	    subject: 'Forgot Password', // Subject line
	    text: 'Your Password is generated.', // plain text body
	    html: '<div align=center><h3>Your New Password</h3><h3>'+randPass+'</h3></div>' // html body
	};

	// send mail with defined transport object
	con.query("SELECT * FROM tbl_users WHERE email=?", [emailAddress], function(err, results1) {
		if(err) {
			res.json({"status":401,"Message": "Error in query."});
		} else {
			if (results1.length > 0) {
				transporter.sendMail(mailOptions, (error, info) => {
				    if (error) {
				       	console.log(error);
				       	res.json({"status":401,"Message":"Problem in sending email... Try again..."});
				    }else {
				    	con.query("UPDATE tbl_users SET password=? WHERE email=?",[encrypt(randPass),emailAddress], function(err, results) {
				    		if (!err) {
				    			res.json({"status":200,"Message": "Your new password has been send to your email address."});
				    		}
				    	});
					    //console.log('Message %s sent: %s', info.messageId, info.response);
				    }
				});
			} else {
				res.json({"status":401,"Message": "Your Email Address is not Registered. Please Try to register."});
			}
		}
	});
});
/*-----------------------------------FORGOT PASSWORD END---------------------------------------*/

// '<a href=http://localhost:3000/forgotPassword/' + token + '>http://localhost:3000/forgotPassword/' + token + '</a>'
//  + " " + "<" + emailAddress + ">"

/*-----------------------------------LOGIN USER START---------------------------------------*/
app.post('/login', urlparser, function(req,res) {
	var password = encrypt(req.body.password);

	con.query("SELECT * FROM tbl_users WHERE email=? AND password=?", [req.body.email, password], function(err1, results1) {
		if(err1) {
            console.log(err1);
            res.json({"status":401,"Message":"Fail to login"});
        }
        else {
        	if (results1.length > 0) {

        		con.query("SELECT * FROM tbl_user_token WHERE userid=?",[results1[0].userid], function(err2, results2) {
        			var token = randomstring.generate();
	        		var date = new Date();
    
        			if (!err2) {
        				if (results2.length > 0) {
        					con.query("UPDATE tbl_user_token SET vToken=? WHERE iTokenId=?",[token,results2[0].iTokenId]);
   						} else {
        					con.query("INSERT INTO tbl_user_token(userid,vToken,created_at) values(?,?,?)",[results1[0].userid,token,date]);
        				}
	        			console.log("Login Successfully.");
				        res.json({"status":200,"Message":results1});
        			}
        		});
        	} else {
        		console.log("Check email or password.");
        		res.json({"status":401,"Message":"Check email or password."});
        	}
        }
	});
});
/*-----------------------------------LOGIN USER END---------------------------------------*/


/*-----------------------------------REGISTER USER START---------------------------------------*/
app.post('/register', urlparser, function(req,res) {
	var password = encrypt(req.body.password);

	con.query("SELECT * FROM tbl_users WHERE email=?",[req.body.email], function(err, emailData) {
        if (!err) {
        	if (emailData.length > 0) {
        		console.log("User Exist.");
	            res.json({"status":401,"Message":"User Already Exist."});
	        } else {
	        	con.query("INSERT INTO tbl_users(name,email,password,mobile,address) values(?,?,?,?,?)", [req.body.name, req.body.email, password, req.body.mobile, req.body.address], function(err, results) {
					if(err){
			            console.log(err);
			            res.json({"status":401,"Message":"Fail to insert"});
			        } else {
						console.log("Registered");
			            res.json({"status":200,"Message":"Successfully Added."});
			        }
        		});
	        }
        }
	});
});
/*-----------------------------------REGISTER USER END---------------------------------------*/


/*-----------------------------------LOGIN USER START---------------------------------------*/
app.get('/view', urlparser, function(req,res) {

	var authentication = req.headers['authentication'];

	con.query("SELECT * FROM tbl_user_token WHERE vToken=?",[authentication], function(err, results1) {
		if(!err) {
            if (results1.length > 0) {

            	con.query("SELECT * FROM tbl_users WHERE userid=?",[results1[0].userid], function(err, results2) {
					if(err){
			            console.log(err);
			            res.json({"status":401,"Message":"Fail to Retrieve data."});
			        }
			        else {
						con.query("SELECT * FROM tbl_users WHERE eIsActive='y'", function(err, results3) {
							if(err) {
					            console.log(err);
					            res.json({"status":401,"Message":"Fail to select"});
					        }
					        else {
					            res.json({"status":200,"Message":results3});
					        }
						});
					}
				});
			} else {
            	res.json({"status":401,"Message":"Please login first."});
            }
		}
	});
});
/*-----------------------------------LOGIN USER END---------------------------------------*/



/*-----------------------------------UPDATE RECORD START---------------------------------------*/
app.post('/update', urlparser, function(req,res) {

	var authentication = req.headers['authentication'];

	con.query("SELECT * FROM tbl_user_token WHERE vToken=?",[authentication], function(err, results1) {
		if(!err) {
            if (results1.length > 0) {

            	con.query("SELECT * FROM tbl_users WHERE userid=?",[results1[0].userid], function(err, results2) {
					if(err) {
			            console.log(err);
			            res.json({"status":401,"Message":"Fail to Retrieve data."});
			        }
			        else {
			        	
			        	var name = (req.body.name == "" ? results2[0].name : req.body.name);
			        	var email = (req.body.email == "" ? results2[0].email : req.body.email);
			        	var address = (req.body.address == "" ? results2[0].address : req.body.address);
			        	var mobile = (req.body.mobile == "" ? results2[0].mobile : req.body.mobile);

						con.query("UPDATE tbl_users SET name=?, email=?, address=?, mobile=? WHERE userid=?", [name,email,address,mobile,results2[0].userid], function(err, results3) {
							if(err) {
					            console.log(err);
					            res.json({"status":401,"Message":"Fail to select"});
					        }
					        else {
					            res.json({"status":200,"Message":"Record has been updated."});
					        }
						});
					}
				});
			} else {
            	res.json({"status":401,"Message":"Please login first."});
            }
		}
	});
});
/*-----------------------------------UPDATE RECORD END---------------------------------------*/


/*-----------------------------------DELETE RECORD START---------------------------------------*/
app.post('/delete', urlparser, function(req,res) {

	var authentication = req.headers['authentication'];

	con.query("SELECT * FROM tbl_user_token WHERE vToken=?",[authentication], function(err, results1) {
		if(!err) {
            if (results1.length > 0) {

            	con.query("SELECT * FROM tbl_users WHERE userid=?",[results1[0].userid], function(err, results2) {
					if(err){
			            console.log(err);
			            res.json({"status":401,"Message":"Fail to Retrieve data."});
			        }
			        else {
			        	if (results2.length > 0) {
			        		con.query("DELETE FROM tbl_users WHERE userid=?",[req.body.userid]);
			        		res.json({"status":200,"Message":"User has been deleted."});
			        	} else {
			        		res.json({"status":401,"Message":"Error in deletion."});
			        	} 
			        }
			    });
			} else {
            	res.json({"status":401,"Message":"Please login first."});
            }
		}
	});
});
/*-----------------------------------DELETE RECORD END---------------------------------------*/


/*-----------------------------------CHANGE STATUS START---------------------------------------*/
app.post('/changeStatus', urlparser, function(req,res) {

	var authentication = req.headers['authentication'];

	con.query("SELECT * FROM tbl_user_token WHERE vToken=?",[authentication], function(err, results1) {
		if(!err) {
            if (results1.length > 0) {

            	con.query("SELECT * FROM tbl_users WHERE userid=?",[results1[0].userid], function(err, results2) {
					if(err){
			            console.log(err);
			            res.json({"status":401,"Message":"Fail to Retrieve status data."});
			        }
			        else {
			        	if (results2.length > 0) {
			        		con.query("UPDATE tbl_users SET eIsActive=? WHERE userid=?",[req.body.status,results1[0].userid]);
			        		res.json({"status":200,"Message":"Status has been changed."});
			        	} else {
			        		res.json({"status":401,"Message":"User Data not Found."});
			        	} 
			        }
			    });
            } else {
            	res.json({"status":401,"Message":"Please login first."});
            }
        }
	});
});
/*-----------------------------------CHANGE STATUS END---------------------------------------*/

/*-----------------------------------CHANGE PASSWORD START---------------------------------------*/
app.post('/changepass', urlparser, function(req,res) {

	var authentication = req.headers['authentication'];

	con.query("SELECT * FROM tbl_user_token WHERE vToken=?",[authentication], function(err, results1) {
		if (!err) {
			if (results1.length > 0) {
			
				var opass = encrypt(req.body.opass);
				var npass = encrypt(req.body.npass);
				var cpass = encrypt(req.body.cpass);

				con.query("SELECT * FROM tbl_users WHERE userid=?",[results1[0].userid], function(err, results2) {
					if(err){
			            console.log(err);
			            res.json({"status":401,"Message":"Fail to Retrieve password data."});
			        }
			        else {
			        	if (opass == results2[0].password) {
			        		if (npass == cpass) {
			        			con.query("UPDATE tbl_users SET password=? WHERE userid=?",[cpass,results1[0].userid], function(err, results3) {
									if(err){
							            console.log(err);
							            res.json({"status":401,"Message":"Error while updating password."});
							        }
							        else {
							        	console.log(err);
							            res.json({"status":200,"Message":"Password has been changed Successfully."});
							        }
			        			});
			        		} else {
						        res.json({"status":401,"Message":"New and Confirm password not matched."});	
				        	}

			        	} else {
					        res.json({"status":401,"Message":"old password incorrect."});	
			        	}
			        }
				});
			} else {
				res.json({"status":401,"Message":"Please login first."});
			}
		} 
	});
});
/*-----------------------------------CHANGE PASSWORD END---------------------------------------*/


app.use(function(req, res, next) {
	var err = new Error('Not Found');
	res.json({"status":404,"Message":"Page You are looking is not available... Sorry... :)"});
});

function encrypt(text) {
  	var cipher = crypto.createCipher(algorithm,password)
  	var crypted = cipher.update(text,'utf8','hex')
  	crypted += cipher.final('hex');
  	return crypted;
}
 
function decrypt(text) {
  	var decipher = crypto.createDecipher(algorithm,password)
  	var dec = decipher.update(text,'hex','utf8')
  	dec += decipher.final('utf8');
  	return dec;
}

module.exports = app;



//var Task = require('./API/models/demoModel.js');
//var Tasks = require('./API/routes/Tasks');
//app.use('/Add',Tasks);
//router.get('/login',Tasks.Login);

